==============================
First Production-Ready Version
==============================

For this milestone, in addition to the list of features below, we will need to
take time to stabilize the previous developments, and write lots of tests to
ensure what we start deploying in production is of sufficient quality.

We will get to this milestone iteratively, releasing regularly after the
initial prototype.

Do note that this will not be a finished product, it only represents what we
would feel comfortable deploying in production, with more coming as updates.

At a minimum, we want to have implemented the following before deploying the
first boxes:

* the new
  `development and deployment workflow and tools <dev-test-deploy.rst>`_

* the `configuration management <configuration.rst>`_

* importing data from a current Ideascube installation

  * this will most likely be a manual operation: someone will run a script and
    babysit it; the script will do as much as it can; we will not automatically
    import data when updating an old box
  * depending of future developments and decisions, this might imply a loss of
    information (hopefully not) or need some extra

* be ready for data migrations in future releases

* a full-featured `stock and inventory management <stock-inventory.rst>`_

* adding support for zims and static sites to the new catalog

* `server sysadmin <sysadmin.rst>`_

  * at this point we need to have a way for the box to connect to the Internet
    and expose a Wi-Fi hotspot
  * the communication with Cockpit will need to be fully fleshed out and secure

* finish the `user management <users-permissions.rst>`_

* integrate external apps like BSFCampus and Kalite

  * in particular, how do these fit in the new home page design

* implement the new `home page <home.rst>`_

  * at least staff persons should be able to curate the home page everybody
    sees, patron-specific home pages would come in the future

* a prototype for `stat monitoring <monitoring.rst>`_
