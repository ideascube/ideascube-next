`◂ Back <README.rst>`_

===========
Online Help
===========

Some operations in Ideascube aren't very easy, particularly for the
`Libraries Without Borders`_ target users, who have often never used a digital
device in their life before.

We should certainly make things as simple as possible with good design, but a
good documentation will often be required nevertheless.

The documentation should be accessible directly in the Ideascube application
itself, because that's where users will search for it anyway. Help articles
should be searchable directly in the global search, just like multimedia
content.

The documentation should cover tasks for both library visitors and staff. Each
page should cover a single task, for example "Connect to the Internet" or
"Search for content", so they better answer the questions that people ask.

Documentation pages should of course be translated.

The documentation should be written in a format that allows rich text (titles,
bold, italics, screenshots, ...) but is still simple to write by non-technical
documentation writers and translators. Markdown/CommonMark might be the least
bad option.

The pages would probably live in a dedicated Git repository, so that writers
and translators don't get confused by all the Ideascube source code. That
repository could then be pulled as a submodule of the Ideascube repository, or
be a dedicated app. (in the sense of composable apps like Django does)

.. _Libraries Without Borders: https://www.librarieswithoutborders.org/
