`◂ Back <README.rst>`_

===========
The Library
===========

At its heart, Ideascube is an appliance for libraries: it manages content and
equipments, presents the former to patrons, lets them borrow it, and enabled
the staff manage the stock of the latter.

Everything else are accessory needs around this primary objective: managing
permissions, pointing towards external applications, or managing even the
server itself.

There are couple of points to consider:

* how the library staff can editorialize/classify the content;
* how we present the content to the patrons, and how they can search for it;

Topics
======

The core of our new editorialization model will be the **topic**:

* a content item can be tagged with as many topics as desired;

  this is in essence a classic tagging system;

* a topic can itself be tagged with as many topics as desired;

  this allows nesting hierarchies, "putting a category inside a category";

* topics become the only way to categorize content;

  once both content items and topics themselves can be tagged with topics, we
  do not need any other categorization mechanism;

We also want a topic to be more than just a simple tag. We want it to have a
long-text description, to show on the page listing the contents of a given
topic. We also want the name and description to be translatable, so that a
Spanish patron would see the topic displayed as "Biología", while a French
patron would see "Biologie".

Items
=====

We want to have **all** content items share a common data model. We do not want
to have external content any more. This allows treating all the content in the
same way, and for example having a view on a category listing books, videos and
zims, which is entirely impossible with the current Ideascube application.

However, while some content items are purely digital (e.g ebooks, videos, …),
some are physical (e.g paper books, DVDs, …). Managing both the same way means
we probably also want to manage all the physical stock together: including
equipments. (e.g chairs, laptops, …)

Thus we need to separate content items. We can do that by having a base
``Item`` model -- with all the common properties like a ``name`` and whether
the item is ``physical`` -- from which a ``Content`` model inherits.

When managing the stock, we would list ``Item``s which are ``physical``.

When displaying contents, we would list all the ``Contents``, whether
physical books or digital content.

This means the association with topics must be done on the ``Content``, not on
the ``Item``, because editorialization doesn't make much sense for chairs or
laptops.

On another note, the separation between an item and a specimen is confusing for
users, and leads to a complex implementation. (see for example the terrible
performance we had in the stock, and how we had to be creative in order to fix
it) Another problem is for book translations: we currently have a single
"Brave New World" book with multiple specimens. Translations are specimens, so
they are related to the same book, but then they all share the same title, in
the language which was used when creating the book object.

As a result, we want to flatten this out completely and merge the two
models, to greatly simplify everything. Each content item would cover both the
current item/specimen attributes. We would then just need an association table
to make some contents related to others.

Data Model
==========

The above leads us to the following data model::

    Topic
    -----
    * id: integer, primary-key
    * slug: text, non-nullable, unique (for the URLs, computed from the name)
    * name: JSON, non-nullable
    * description: JSON, non-nullable
    
    Item
    ----
    * id: integer, primary-key
    * physical: boolean, non-nullable
    * barcode: text, nullable, unique
    * quantity: integer, non-nullable, default 1
    * name and other metadata common to all content and equipment items
    
    Content
    -------
    * inherits from Item
    * author and other metadata related to content items (all optional)

    TopicToContentAssociation
    -------------------------
    * topic_id
    * tagged_id

    TopicToTopicAssociation
    -----------------------
    * topic_id
    * tagged_id

The ``name`` of a topic would be something like::

    {
      "en": "Biology",
      "es": "Biología",
      "fr": "Biologie",
    }

The view would display the one corresponding to the language chosen by the user
for the UI, falling back on the default language for the library.

When creating new tags, the staff would need to enter at least one name, for
the default language of the library. They could add translations, but wouldn't
necessarily have to.

What Patrons See
================

Patrons don't see anything related to the equipments (chairs, laptops, …), they
are only interested in the content.

The whole navigation is based on the topics. As such we need only two views:

* a topic view, which displays a list of what has been tagged with this topic;

  the list contains both other topics and content items;

* an item view, which displays the metadata of the item;

  if the item is a video, it can be played directly in this view, if the item
  is a PDF, it can be read directly in this view, etc…

  digital items can be downloaded, so the patron can consult them later on
  their own device;

  physical books get displayed just like digital content, which means we do not
  need a special "Library" entry point just for them any more, removing a big
  source of confusion;

The home page doesn't list all topics or content items, but instead it will be
a curated list of things the library staff want people to have a direct access
to. Eventually patrons could add things to their own view of the home page as
well.

A breadcrumb needs to be built as the patron browses through topics to a given
content item. Since topics can be tagged with other topics, it will be possible
to arrive at a given view from multiple URLs:

* ``/topic2/`` and ``/topic1/topic2/`` would show the exact same thing, if
  ``topic2`` is tagged with ``topic1``;

* ``/item1/`` and ``/topic1/topic2/item1`` would show the exact same thing, if
  ``item1`` is tagged with ``topic2`` and ``topic2`` is tagged with ``topic1``;

This shouldn't be a problem really: the breadcrumb serves to come back on the
path a given user has been following, and with a complex editorialization it is
not surprising that multiple paths can lead to the same content.

An important detail is that the URLs will use the slugs of topics and content
items, not their numeric IDs, as opposed to what we currently do in Ideascube.

Content Management
==================

Managing the content should remain as simple as possible, otherwise library
staff just won't do it, and they will never benefit from Ideascube in the
fullest.

A generic "Add content" form where the staff would have to choose the type,
then enter the appropriate fields but leave out the irrelevant one would most
likely be too intimidating.

Instead, we would probably do better to our users with dedicated forms:

* Add a book
* Add a video
* Add a zim
* …

Each form would be tailored to the fields that are truly relevant for each
content type.

Most metadata fields should be optional, to make it as easy as possible to
add content into Ideascube. Of course, if the staff enrich their content with
more metadata, it will make the patron experience richer: they will be able to
find content much more easily, and we would be able to display more interesting
information about each content item.
