`◂ Back <README.rst>`_

=========
Prototype
=========

This will **not** be a version which should be deployed in production.
Specifically, for this version we will **not** handle any kind of migration,
whether from the current Ideascube to this prototype, or from this prototype to
future versions.

Here are the things this prototype will have:

* `the multimedia library system <library.rst>`_

  * basic management of content items and topics
  * views to display content items and topics
  * a basic home page, listing all topics

* `catalog management <catalog.rst>`_

  * for this milestone we will port the CLI we have in the current Ideascube to
    the new catalog design
  * we might only handle medias and topics, not zims and static sites

* `user management <users-permissions.rst>`_

  * just the basics for now, at least the parts needed to implement the rest

* `stock management <stock-inventory.rst>`_

  * at this point we will just list physical items, and allow creating them

* loans

  * this will be very close to what we have in the current Ideascube, as it
    works well, and is quite simple to port

* `server sysadmin <sysadmin.rst>`_

  * in this milestone we will do everything needed to shutdown and restart the
    server, using Cockpit
  * the communication with Cockpit might not be completely fleshed-out at this
    point
