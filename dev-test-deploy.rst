`◂ Back <README.rst>`_

=========================================
Development, Test and Deployment Workflow
=========================================

Our workflow on Ideascube is a very classic one: we develop in a virtualenv,
run the tests in the CI in a virtualenv, then package a whole virtualenv as a
Debian package which we install on the boxes with Ansible.

This has a few problems:

* Setting up a development environment is cumbersome: a new developer is
  supposed to create a virtualenv, install the dependencies with ``pip``, which
  fails because they are missing some development headers, so they install it,
  repeat a few times for other development headers, …

* Each environment is different: each developer (and the CI) installs and
  builds the dependencies in their own virtualenv, with different versions of
  the compiler, system libraries, etc. This leads to problems which are
  incredibly hard to debug, because they only happen in certain environments.

  In addition, it means we waste a lot of CPU time on compiling things which
  have already been compiled. For example, at each release our ARM builder
  spends about 20 minutes compiling ``lxml``, even when we haven't bumped the
  version since the last release build.

* Boxes download too much when updating: because our only release artifact is
  a monolithic Debian package, each update requires downloading the whole
  package, roughly 20MB, even if only a few files changed since the last
  release.

  In addition, the Debian package takes a long time to extract on our ARM boxes
  (it is a compressed archive after all) even though most files in it already
  have an identical copy installed on the system, from the previous version.

* Deploying Ideascube on a box is way too complex, involving too many steps on
  too many moving parts.

* We know we will need custom applications in some deployments (e.g the RFI
  radio project), but we have no idea how we would build and deploy releases
  for those.

We really ought to do better. A possible solution could be built on top of a
few third-party technologies.

BuildStream
===========

`BuildStream <https://buildstream.gitlab.io/buildstream/>`_ can allow us to
build bootable images with:

* a base OS;
* a container bundling Ideascube and its dependencies.

Such an image is **trivial to deploy**. (just ``dd`` the image file onto the
disk)

The container for Ideascube is updatable separately from the OS.

BuildStream also gives us a cache of build artifacts which can be shared
between:

* developer workstations;
* continuous integration runners;

This means **we never need to build anything which was already built**. It
becomes extremely **quick and easy to set up a new development environment**:

1. install BuildStream, OSTree and Bubblewrap;
2. clone the Ideascube repository;
3. build the project.

BuildStream will find the already built artifacts in the shared cache, and just
download them. At that point a new developer has everything needed to make
changes, and run the tests: BuildStream does all of that in sandboxes which
contain Ideascube, its dependencies, and the test framework.

We can even build an SDK which will contain all the tools necessary for
development, and a Platform which only contains what is necessary to run the
application; both will share the same exact bits. This way we can have multiple
applications all based on the same SDK/Platform, without needing to rebuild the
shared parts.

This also dramatically simplifies the job of the CI: it starts from a clean
environment, download everything which hasn't changed since the last build and
only builds what has. We can imagine running the unit tests of the apps in the
SDK, but the integration tests would run in the Platform.

OSTree and Bubblewrap
=====================

`OSTree <https://ostree.readthedocs.io/en/latest/>`_ is a pretty great
mechanism to handle the updates of the Ideascube container, as it gains us
**differential upgrades where the boxes only download the changed files**.

However that doesn't seem compatible with the top container technologies
(mostly Docker) so it needs something ad-hoc like what Flatpak does with
`Bubblewrap <https://github.com/projectatomic/bubblewrap>`_.

We could use OSTree for the OS as well, like what
`Project Atomic <https://www.projectatomic.io/>`_ does. At that point we get
atomic, differential updates for the OS. This is especially important for
things like the recent `attacks on WPA2 <https://www.krackattacks.com/>`_.

And those OSTree repos on the box (the OS and the app) could even have a NGinx
in front, serving those bits as static content for other boxes to consume. We
thus get **peer-to-peer updates of both the apps and the OS**.

Continuous Integration
======================

We would need to deploy our own Gitlab CI runner, but it would gain us a very
interesting workflow:

1. a developer makes changes to the application, and pushes them to a new
   branch;
2. the CI builds the branch with BuildStream, then runs the tests;
3. any developer can checkout the branch locally, build it with BuildStream,
   which will not actually build anything: BuildStream will just download the
   build artifacts from the shared cache; this makes it **trivial to test
   someone else's changes**, even when they include adding new dependencies;
4. eventually the branch is merged into ``master``;
5. the CI builds ``master`` with BuildStream, which again doesn't actually do
   anything as the build-artifacts are already in the shared cache; (they were
   built on the just-merged branch)
6. on ``master``, the CI will also publish the built application to the
   ``devel`` branch of the public OSTree repository; this makes it **trivial
   for anybody to test the latest changes from ``master``, even on production
   boxes**: they can just pull from the ``devel`` branch;
7. when we want to make a release, we can just take the latest ``devel`` commit
   in the public OSTree repository, and re-commit it to the ``stable`` branch;
   this means we do not rebuild even for the release, we just **release the
   exact bits which were tested**;
8. production boxes would normally pull updates from the ``stable`` branch of the
   public OSTree repository; they can of course have both the stable and devel
   versions installed at the same time;

This neatly solves all the problems we are currently facing:

* it is trivial to set up a new development environment;
* everybody runs the same bits, from developer workstation to production boxes
  through the CI, nobody rebuilds what has already been built;
* updates involve only downloading the changes files; updates are also atomic,
  and rolling back is trivial; (at least for the code, database migrations are
  still as hard as usual)
* deploying a new box is easy: just ``dd`` the whole image on a disk, then
  Ansible can still be used to customize things afterwards;
* custom applications are just new references in the OSTree repository, built
  the same way as the main Ideascube application;

We could even have a small tooling to glue all of this together and make it
simpler to work with. The BuildStream, OSTree and Bubblewrap commands to run
can quickly become very complex, such a tool would abstract all that.
