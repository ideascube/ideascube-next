`◂ Back <README.rst>`_

=========
Home Page
=========

With the `new editorialization <library.rst>`_ based entirely around topics,
and the merge of the library and mediacenter applications, the Ideascube home
page will change a lot compared to what we have.

We want to let the library staff configure what appears on the home page. They
should be able to select a list of "cards" pointing to either:

* a topic;
* a media;
* a custom URL, either inside or outside Ideascube;

Cards will need a few metadata in order to have a nicer home page:

* a title; for topics and medias, this would be their own title;
* a description; for topics and medias, this would be their own description;
* an optional picture thumbnail;
* an optional classification; this will be one of ``creativity``,
  ``entertainment``, ``culture``, ``vocational training``, ``education`` or
  ``information``;

We do not want to implement a drag-and-drop layout manager. Home page
configuration will be limited to choosing which cards to display, and in which
order.

A design proposal for Ideascube shows complex layouts with some cards occupying
a single "square space" and others occupying two "square spaces" when they have
a thumbnail. While beautiful, this would be a nightmare to implement.
Thumbnails will be displayed as the background of the card, with an effect
applied (blur, coloring, ...) to make the text on top readable.

Deployers should be able to preseed the home page with a few cards.

Eventually, we want to allow (logged in) patrons to refine their own home page.

To this end, they will be able to create their own cards which will be added to
a "My Favorites" zone. That zone would probably be displayed above the list of
global cards chosen by the staff.

Another interesting feature would be to have "per-activity" cards: the staff
could create cards which are visible by all the patrons taking part in an
activity, so they could start the activity without losing too much time.
