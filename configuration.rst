`◂ Back <README.rst>`_

========================
Configuration Management
========================

We want three types of configurations in Ideascube:

1. options targetted at the library staff, which govern the behaviour of the
   application, what it presents to users and how it presents it;
2. user preferences, influencing what a given user sees when they log in;
3. options targetted at the system administrators deploying Ideascube;

Each should be handled differently. The rest of this page details how each
should work, and suggests a possible implementation.

.. _library-configuration:

Library Configuration
=====================

These options control the way users experience the application. A few examples
would be:

* the name of the library (which we currently call ``IDEASCUBE_NAME``);
* the list of cards to show on the home page;
* the default language for the UI;

As these can be modified in the graphical interface by the staff, they must be
stored in the application database, with a dedicated ``Configuration`` model.

User Preferences
================

*TODO:*

* this requires users logging in, which BSF/Kiwix deployments don't do;
* that seems like something important for other use-cases though (e.g social
  centers, MJCs, ...) so we need to at least have a decent idea of how this
  could look like;
* anonymous users would still be able to browse the content, but just would
  be able to change their preferences;

Application Deployment Configuration
====================================

These options control the way the application is deployed and runs on the
system. They are for the system administrators of the machine, who might not be
the same people as the library staff. A few examples would be:

* the URL to connect to the database;
* the path to the media storage;

These are only for the system administrators to modify, and therefore should
live in the ``/etc/ideascube/config.ini`` file. *(note: the file format is
given as an example only, we haven't chosen what format to actually use yet)*

Good defaults should be provided so that the application can run without any
configuration file, at least as much as is reasonably possible. (for example
for cryptographic secrets, we could either decide that they are required to be
set in the file, the app refusing to run otherwise, or we could instead emit
very loud warnings if the default is used)

Those options would generally be set only once when the application is
deployed initially, new options being added eventually with application
updates. Changing these options requires restarting the application.
