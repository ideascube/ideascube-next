`◂ Back <README.rst>`_

=============
Search System
=============


Patrons will mainly navigate through the content in two ways:

- Navigation over topics,
- Search of content.

The search system will provide the same search functionality as the one in the
current Ideascube but it will be extended to all content types (book, zims,
blog, ...)

Most of the search will be carried through PostgreSQL queries.
As we will use a unified model to store all the content, we will not need a
separate index table.

This leads us to the question of how to search in external content, such as:

- zim: we want to serve zim files ourselves by wrapping libzim/kiwix-lib, but
  the full-text index will still be external; (xapian)
- blog articles: we are not sure of what the blog will become and we have
  chosen to keep it as a separate model for now;
- static sites, Kalite, Kolibri, …

One solution would be to have a mechanism of search providers similar to the
`Gnome system <https://developer.gnome.org/SearchProvider/>`_

Search providers would provide a web API to respond to search request and
return a standardized result (json, yaml or xml, maybe OpenSearch?).

However, this solution is not perfect and leads to new questions to answer.

How we run the query?

It can be made by the backend or the frontend:

- If this is the backend, we (probably) have to do it synchronously. If a
  search provider takes time to respond (or is down), the user request can be
  stalled, and time out.
- If this is the frontend, we will do it asynchronously but it leads to UX
  design problems: we don't want search results coming later to "push"
  existing content down. It would be pretty annoying that a result moves just
  before a patron tries to click on it.

The "ideascube research" (research on our content) can be directly made using
postgresql queries. But if we are defining our research system as a collection
of search providers, we need to create a ideascube provider instead.
It is not a problem by itself, but if the search is done by the backend, do
we really want the backend to use a web endpoint to do a search it could do
directly?
