`◂ Back <README.rst>`_

=============
Initial Setup
=============

When the application is started for the first time, the first person connecting
to it should be greeted by the initial setup wizard.

It should have only a few pages to :ref:`configure the library`. It shouldn't
be too long, configuring everything possible, but only those options which are
important enough to present to a first time staff, for example:

1. the default language of the UI; (which is then used for the rest of the
   pages in the initial setup wizard)
2. the credentials of an omnipotent staff account, with which the person can
   log in;
3. the name of the library;
4. an optional Wi-Fi network to connect to, so the box can access the Internet;

It should be possible to pre-seed those options when deploying a new box. A
simple way to achieve that could be the following:

* when the application starts, it checks whether a staff already went through
  the initial setup (this could be the
  ``/var/lib/ideascube/initial-setup-done`` file, or something stored in the
  database); if so, the initial setup wizard is entirely skipped;
* when the initial setup wizard starts, it checks for the
  ``/etc/ideascube/initial-setup.ini`` file; if it exists, the options it
  contains are automatically set as if the staff person had set them in the
  initial setup wizard; the corresponding pages of the initial setup wizard are
  skipped;
* when the initial setup wizard finishes, it stores the fact someone went
  through it, so that restarting the application in the future skips the
  wizard;
