===============
A New Ideascube
===============

This repository contains our thoughts on what the future could be for
Ideascube.

We share it in the hope that others will find interesting, and also because it
allows us to work on it collaboratively.


Architecture and Designs
========================

* `The Library <library.rst>`_
* `Getting Remote Content <catalog.rst>`_
* `Development, Test and Deployment Workflow <dev-test-deploy.rst>`_
* `Online Help <help.rst>`_
* `Configuration Management <configuration.rst>`_
* `Initial Setup <initial-setup.rst>`_
* `Search system <search.rst>`_


Roadmap
=======

We want to have a first rough `prototype <roadmap-prototype.rst>`_ as soon as
possible.

We will then add features and fix bugs iteratively, releasing regularly, until
we get to the first `production-ready version <roadmap-production-ready.rst>`_.
